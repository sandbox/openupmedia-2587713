<?php
/**
 * @file
 * The main template for the Guestcounter module.
 *
 * Variables available:
 * - $guestcount: An integer representing the amount of visitors on the website.
 */
?>
<div class="guestcount">
  <?php print t('!guestcount guest(s) online', array('!guestcount' => $guestcount)); ?>
</div>
